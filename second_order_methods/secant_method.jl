function secant_method(f', x0, x1, ϵ)
    g0 = f'(x0)
    Δ = Inf
    while abs(Δ) > ϵ
        g1 = f'(x1)
        Δ = (x1 - x0) / (g1 - g0) * g1
        x0, x1, g0 = x1, x1 - Δ, g1
    end
    return x1
end
