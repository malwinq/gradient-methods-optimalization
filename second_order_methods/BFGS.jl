mutable struct BFGS <: DescentMethod
    Q
end

function init!(M::BFGS, f, ∇f, x)
    m = length(x)
    M.Q = Matrix(1.0I, m, m)
    return M
end

function step!(M::BFGS, f, ∇f, x)
    Q, g = M.Q, ∇f(x)
    x' = line_search(f, x, -Q*g)
    g' = ∇f(x')
    δ = x' - x
    γ = g' - g
    Q[:] = Q - (δ * γ' * Q + Q * γ * δ') / (δ' * γ) +
           (1 + (γ' * Q * γ) / (δ' * γ))[1] * (δ * δ') / (δ' * γ)
    return x'
end
