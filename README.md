Warsaw University of Technology, Faculty of Electrical Engineering

Applied Computer Science, 2020

# Gradient methods optimalization

Analysis and optimalization of some gradient calculations algorithms in Julia language

The goal is to validate the implemtations of algorithms for gradient calculations:
*   BFGS
*   L-BFGS (Limited memory BFGS)
*   Adam

The algorithms are tested on loss functions:
*   2D Rosenbrock function
*   Wheeler's Ridge function (2D)
*   4D Michalewicz function
*   6D Rosenbrock function
*   dense neural network

## Libraries

Used libraries with their version:
*  Plots v1.2.0
*  PGFPlots v3.2.1
*  BenchmarkTools v0.5.0
*  LinearAlgebra
*  Flux v0.10.4
*  Vec v2.0.2

The project is written in LaTex.

## Runtime parameters

*  RAM memory: 16 GB, DDR4
*  Processor: Intel Core i7-7Y75, 2 cores, 4 threads, 4M Cache, up to 3,6 GHz
*  Disk: 1000GB SSD M.2 NVMe
*  Operating system: Windows 10
*  IDE: Atom with Juno plug-in for Julia language

## Structure

*  scripts - code for evaluating the algorithms
*  plots - images of plots
*  test_functions, first_order_methods, second_order_methods - functions

## Theory:
Mykel J. Kochenderfer, Tim A. Wheeler, 2019, Algorithms for Optimization, MIT Press
