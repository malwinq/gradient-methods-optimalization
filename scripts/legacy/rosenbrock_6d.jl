abstract type DescentMethod end

using LinearAlgebra
using Vec
	function _line_search(f, x, d)
	    d = normalize(d)
	    objective = α -> f(x + α*d)
	    v, α = f(x), 1e-6
	    while f(x + α*d) < v
	        v = f(x + α*d)
	        α += 1e-6
	    end
	    return x + α*d
	end

	mutable struct Adam <: DescentMethod
	    α       # learning rate
	    γv      # update decay
	    γs      # gradient decay
	    ϵ       # very small value
	    k       # step
	    v       # 1st moment estimate
	    s       # 2nd moment estimate
		# Adam() = new()
	end

	function init!(M::Adam, f, ∇f, x)
	    M.k = 0
	    M.s = zeros(length(x))
	    M.v = zeros(length(x))
	    return M
	end

	function step!(M::Adam, f, ∇f, x)
	    α, γv, γs, ϵ, k = M.α, M.γv, M.γs, M.ϵ, M.k
	    s, v, g = M.s, M.v, ∇f(x)
	    s[:] = γs * s + (1.0 - γs)*(g .* g)
	    v[:] = γv * v + (1.0 - γv)*g
	    M.k = k += 1
	    v_hat = v ./ (1.0 - γv^k)
	    s_hat = s ./ (1.0 - γs^k)
	    return x - α*v_hat ./ (sqrt.(s_hat) .+ ϵ)
	end

	mutable struct BFGS <: DescentMethod
	    Q
		# BFGS() = new()
	end

	function init!(M::BFGS, f, ∇f, x)
	    m = length(x)
	    M.Q = Matrix(1.0I, m, m)
	    return M
	end

	function step!(M::BFGS, f, ∇f, x)
	    Q, g = M.Q, ∇f(x)
	    x′ = _line_search(f, x, -Q*g)
	    g′ = ∇f(x′)
	    δ = x′ - x
	    γ = g′ - g
	    Q[:] = Q - (δ * γ' * Q + Q * γ * δ') / (δ' * γ) +
	           (1 + (γ' * Q * γ) / (δ' * γ))[1] * (δ * δ') / (δ' * γ)
	    return x′
	end

	mutable struct LimitedMemoryBFGS <: DescentMethod
		m
		δs
		γs
		qs
		# LimitedMemoryBFGS() = new()
	end

	function init!(M::LimitedMemoryBFGS, f, ∇f, x, m=2)
		M.δs = []
		M.γs = []
	    M.qs = []
		M.m = m
		return M
	end

	function step!(M::LimitedMemoryBFGS, f, ∇f, x)
	    δs, γs, qs, g = M.δs, M.γs, M.qs, ∇f(x)
	    m = length(δs)
	    if m > 0
	        q = g
	        for i in m : -1 : 1
	            qs[i] = copy(q)
	            q -= (δs[i]⋅q)/(γs[i]⋅δs[i])*γs[i]
	        end
	        z = (γs[m] .* δs[m] .* q) / (γs[m]⋅γs[m])
	        for i in 1 : m
	            z += δs[i]*(δs[i]⋅qs[i] - γs[i]⋅z)/(γs[i]⋅δs[i])
	        end
	        x′ = _line_search(f, x, -z)
	    else
	        x′ = _line_search(f, x, -g)
	    end
	    g′ = ∇f(x′)
	    push!(δs, x′ - x); push!(γs, g′ - g)
	    push!(qs, zeros(length(x)))
	    while length(δs) > M.m
	        popfirst!(δs); popfirst!(γs); popfirst!(qs)
	    end
	    return x′
	end

	function run_descent_method(M::DescentMethod, x₀, N::Int32)
	    pts = [x₀]
		iters = []
	    init!(M, f, ∇f, x₀)
	    for i in 1 : N
	        push!(pts, step!(M, f, ∇f, pts[end]))
			# push!(iters, f(pts[end]))
	    end
		# println(f(pts[end]))
	    return iters
	end

	# ROSENBROCK 6D
	f = x ->   (100*(x[1]^2-x[2])^2 + (x[1]-1)^2 +
				100*(x[3]^2-x[4])^2 + (x[3]-1)^2 +
				100*(x[5]^2-x[6])^2 + (x[5]-1)^2)

	∇f = x ->  [100*(4*x[1]^3-4*x[2]*x[1]) + x[1]^2-2*x[1]+1, 100*(2*x[1]^2+2*x[2]),
				100*(4*x[3]^3-4*x[4]*x[3]) + x[3]^2-2*x[3]+1, 100*(2*x[3]^2+2*x[4]),
				100*(4*x[5]^3-4*x[6]*x[5]) + x[5]^2-2*x[5]+1, 100*(2*x[5]^2+2*x[6])]

	x₀ = [-0.5, 1.5, -2, 0.5, -1, 0.8]

	stuff = Tuple{DescentMethod, String, Int32}[]
	push!(stuff, (Adam(0.1, 0.9, 0.8, 1e-8, 0, zeros(2), zeros(2)), "Adam (lr=0.1)", 104))
	push!(stuff, (Adam(0.01, 0.9, 0.8, 1e-8, 0, zeros(2), zeros(2)), "Adam (lr=0.01)", 393))
	push!(stuff, (Adam(0.001, 0.9, 0.8, 1e-8, 0, zeros(2), zeros(2)), "Adam (lr=0.001)", 1873))
	push!(stuff, (BFGS(Matrix(1.0I, 2, 2)), "BFGS", 23))
	push!(stuff, (LimitedMemoryBFGS(1, [], [], []), "L-BFGS (m=1)", 4924657))
	push!(stuff, (LimitedMemoryBFGS(2, [], [], []), "L-BFGS (m=2)", 4924657))
	push!(stuff, (LimitedMemoryBFGS(3, [], [], []), "L-BFGS (m=3)", 4924657))

	println("--------------------------------------------------------------")
	for (M, name, N) in stuff
		println(name)
	    @btime pts = run_descent_method(M, x₀, N)
	end

	# using Plots
	# 	plt = plot()
	# 	for (M, name, N) in stuff
	# 		println(name)
	# 		iters = run_descent_method(M, x₀, N)
	# 		plt = plot!(iters, xlabel="Liczba iteracji", ylabel="Wartość funkcji celu",
	# 				label = name,
	# 				ylims=(0, 40)
	# 				)
	# 		display(plt)
	# 		# savefig("plots/michalewicz_4d/rosenbrock6_adam_bfgs.png")
	# 	end
