abstract type DescentMethod end
using Plots
using LinearAlgebra
using Vec

	function _line_search(f, x::Array{Float64, 1}, d)
	    d = normalize(d)
	    objective = α -> f(x + α*d)
	    v, α = f(x), 1e-6
	    while f(x + α*d) < v
	        v = f(x + α*d)
	        α += 1e-6
	    end
	    return x + α*d
	end

	mutable struct Adam <: DescentMethod
	    α       # learning rate
	    γv      # update decay
	    γs     # gradient decay
	    ϵ      # very small value
	    k::Int32         # step
	    v       # 1st moment estimate
	    s       # 2nd moment estimate
	end

	function init!(M::Adam, f, ∇f, x::Array{Float64, 1})
	    M.k = Int32(0)
	    M.s = zeros(Float64, length(x))
	    M.v = zeros(Float64, length(x))
	    return M
	end

	function step!(M::Adam, f, ∇f, x::Array{Float64, 1})
		α, γv, γs, ϵ, k = Float16(M.α), Float32(M.γv), Float32(M.γs), Float64(M.ϵ), Int32(M.k)
		print(typeof(α))
		s, v, g = M.s, M.v, ∇f(x)
	    s[:] = γs * s + (1.0 - γs)*(g .* g)
	    v[:] = γv * v + (1.0 - γv)*g
	    M.k = Int32(k += 1)
	    v_hat = v ./ (1.0 - γv^k)
	    s_hat = s ./ (1.0 - γs^k)
	    return x - α*v_hat ./ (sqrt.(s_hat) .+ ϵ)
	end

	mutable struct BFGS <: DescentMethod
	    Q
	end

	function init!(M::BFGS, f, ∇f, x::Array{Float64, 1})
	    m = length(x)
	    M.Q = Matrix(1.0I, m, m)
	    return M
	end

	function step!(M::BFGS, f, ∇f, x::Array{Float64, 1})
	    Q, g = M.Q, ∇f(x)
	    x′ = _line_search(f, x, -Q*g)
	    g′ = ∇f(x′)
	    δ = x′ - x
	    γ = g′ - g
	    Q[:] = Q - (δ * γ' * Q + Q * γ * δ') / (δ' * γ) +
	           (1 + (γ' * Q * γ) / (δ' * γ))[1] * (δ * δ') / (δ' * γ)
	    return x′
	end

	mutable struct LimitedMemoryBFGS <: DescentMethod
		m
		δs
		γs
		qs
	end

	function init!(M::LimitedMemoryBFGS, f, ∇f, x::Array{Float64, 1}, m=2)
		M.δs = []
		M.γs = []
	    M.qs = []
		M.m = m
		return M
	end

	function step!(M::LimitedMemoryBFGS, f, ∇f, x::Array{Float64, 1})
	    δs, γs, qs, g = M.δs, M.γs, M.qs, ∇f(x)
	    m = length(δs)
	    if m > 0
	        q = g
	        for i in m : -1 : 1
	            qs[i] = copy(q)
	            q -= (δs[i]⋅q)/(γs[i]⋅δs[i])*γs[i]
	        end
	        z = (γs[m] .* δs[m] .* q) / (γs[m]⋅γs[m])
	        for i in 1 : m
	            z += δs[i]*(δs[i]⋅qs[i] - γs[i]⋅z)/(γs[i]⋅δs[i])
	        end
	        x′ = _line_search(f, x, -z)
	    else
	        x′ = _line_search(f, x, -g)
	    end
	    g′ = ∇f(x′)
	    push!(δs, x′ - x); push!(γs, g′ - g)
	    push!(qs, zeros(length(x)))
	    while length(δs) > M.m
	        popfirst!(δs); popfirst!(γs); popfirst!(qs)
	    end
	    return x′
	end

	function this_step!(M::DescentMethod, v::VecE2{Float64})
		x = Float64[v.x, v.y]
		return VecE2{Float64}(step!(M, f, ∇f, x)...)
	end

	function run_descent_method(M::DescentMethod, x₀::VecE2{Float64}, N::Int16)
		pts = [x₀]
		iters = []
		init!(M, f, ∇f, convert(Vector{Float64}, x₀))
		for i in 1 : N
			push!(pts, step!(M, pts[end]))
			# println(f(pts[end]))
			# push!(iters, f(pts[end]))
		end
		return iters
	end

	# ROSENBROCK
	f = x -> (1-x[1])^2 + 100*(4x[2] - x[1]^2)^2
	∇f = x -> [2*(200x[1]^3 - 800x[1]*x[2] + x[1] - 1), -800*(x[1]^2 - 4x[2])]

	x₀ = VecE2{Float64}(-2,1.5)
	N = Int16(210)

	stuff = Tuple{DescentMethod, String}[]
	push!(stuff, (Adam(Float16(0.1), Float16(0.9), Float16(0.8), Float64(1e-8), Int32(0), zeros(Float64, 2), zeros(Float64, 2)), "Adam, lr=0.1"))
	push!(stuff, (Adam(Float16(0.01), Float16(0.9), Float16(0.8), Float64(1e-8), Int32(0), zeros(Float64, 2), zeros(Float64, 2)), "Adam, lr=0.01"))
	# push!(stuff, (Adam(0.001, 0.9, 0.8, 1e-8, 0, zeros(Float64, 2), zeros(Float64, 2)), "Adam, lr=0.001"))
	# push!(stuff, (BFGS(Matrix(1.0I, 2, 2)), "BFGS"))
	# push!(stuff, (LimitedMemoryBFGS(1, [], [], []), "L-BFGS (m=1)"))
	# push!(stuff, (LimitedMemoryBFGS(2, [], [], []), "L-BFGS (m=2)"))
	# push!(stuff, (LimitedMemoryBFGS(3, [], [], []), "L-BFGS (m=3)"))

	println("--------------------------------------------------------------")
	for (M, name) in stuff
		println(name)
		@time pts = run_descent_method(M, x₀, N)
	end

	# plt = plot()
	# for (M, name) in stuff
	#     iters = run_descent_method(M, x₀, N)
	# 	plt = plot!(iters, xlabel="Liczba iteracji", ylabel="Wartość funkcji celu",
	# 			label = name,
	# 			ylims=(-0.00000000000000005, 0.005)
	# 			)
	# 	# push!(plt, plot(iters))
	# 	display(plt)
	# 	savefig("plots/rosenbrock_2d/results_adam_001.png")
	# end
