##################################################
#
# This script uses Bartosz Chaber's functions
# https://github.com/bchaber/multilayer-perceptron
#
##################################################

import Random: shuffle!, shuffle
include("dual.jl")
abstract type DescentMethod end

using LinearAlgebra
using Vec
	function _line_search(f, x, d)
	    d = normalize(d)
	    objective = α -> f(x + α*d)
	    v, α = f(x), 1e-6
	    while f(x + α*d) < v
	        v = f(x + α*d)
	        α += 1e-6
	    end
	    return x + α*d
	end

	mutable struct Adam <: DescentMethod
	    α       # learning rate
	    γv      # update decay
	    γs      # gradient decay
	    ϵ       # very small value
	    k       # step
	    v       # 1st moment estimate
	    s       # 2nd moment estimate
		# Adam() = new()
	end

	function init!(M::Adam, f, ∇f, x)
	    M.k = 0
	    M.s = zeros(length(x))
	    M.v = zeros(length(x))
	    return M
	end

	function step!(M::Adam, f, ∇f, x)
	    α, γv, γs, ϵ, k = M.α, M.γv, M.γs, M.ϵ, M.k
	    s, v, g = M.s, M.v, ∇f(x)
	    s[:] = γs * s + (1.0 - γs)*(g .* g)
	    v[:] = γv * v + (1.0 - γv)*g
	    M.k = k += 1
	    v_hat = v ./ (1.0 - γv^k)
	    s_hat = s ./ (1.0 - γs^k)
	    return x - α*v_hat ./ (sqrt.(s_hat) .+ ϵ)
	end

	mutable struct BFGS <: DescentMethod
	    Q
		# BFGS() = new()
	end

	function init!(M::BFGS, f, ∇f, x)
	    m = length(x)
	    M.Q = Matrix(1.0I, m, m)
	    return M
	end

	function step!(M::BFGS, f, ∇f, x)
	    Q, g = M.Q, ∇f(x)
	    x′ = _line_search(f, x, -Q*g)
	    g′ = ∇f(x′)
	    δ = x′ - x
	    γ = g′ - g
	    Q[:] = Q - (δ * γ' * Q + Q * γ * δ') / (δ' * γ) +
	           (1 + (γ' * Q * γ) / (δ' * γ))[1] * (δ * δ') / (δ' * γ)
	    return x′
	end

	mutable struct LimitedMemoryBFGS <: DescentMethod
		m
		δs
		γs
		qs
		# LimitedMemoryBFGS() = new()
	end

	function init!(M::LimitedMemoryBFGS, f, ∇f, x, m=2)
		M.δs = []
		M.γs = []
	    M.qs = []
		M.m = m
		return M
	end

	function step!(M::LimitedMemoryBFGS, f, ∇f, x)
	    δs, γs, qs, g = M.δs, M.γs, M.qs, ∇f(x)
	    m = length(δs)
	    if m > 0
	        q = g
	        for i in m : -1 : 1
	            qs[i] = copy(q)
	            q -= (δs[i]⋅q)/(γs[i]⋅δs[i])*γs[i]
	        end
	        z = (γs[m] .* δs[m] .* q) / (γs[m]⋅γs[m])
	        for i in 1 : m
	            z += δs[i]*(δs[i]⋅qs[i] - γs[i]⋅z)/(γs[i]⋅δs[i])
	        end
	        x′ = _line_search(f, x, -z)
	    else
	        x′ = _line_search(f, x, -g)
	    end
	    g′ = ∇f(x′)
	    push!(δs, x′ - x); push!(γs, g′ - g)
	    push!(qs, zeros(length(x)))
	    while length(δs) > M.m
	        popfirst!(δs); popfirst!(γs); popfirst!(qs)
	    end
	    return x′
	end

	function run_descent_method(M::DescentMethod, N::Int32)
		global optimizer = M
		iters = []
		for i=1:N
		  shuffle!(train_set)
		  optimize!(parameters)
		  # push!(iters, test(parameters, 1:data_size))
		end
		# println("Final loss: ", test(parameters, 1:data_size))
	    return iters
	end

	function subviews(u, ns...)
	   vs, s = [], 0
	   for n in ns
	     N = prod(n)
	     v = reshape(view(u, (1:N) .+ s), n...)
	     s += N
	     push!(vs, v)
	   end
	   vs
	end

	function neural_network(x, wh, bh, wo, bo)
	    x̂ = dense(wh, bh, n_hidden, n_input, x, ReLU)
	    ŷ = dense(wo, bo, n_output, n_hidden, x̂, linear)
	end

	function loss(x, y, wh, bh, wo, bo)
		ŷ = neural_network(x, wh, bh, wo, bo)
		mean_squared_loss(y, ŷ)
	end

	function train(parameters, train_set)
	  wh, bh, wo, bo = subviews(parameters,
							    (n_hidden * n_input), (n_hidden),
							    (n_output * n_hidden), (n_output))
	  ∇E = zeros(length(parameters))
	  for j = train_set
	    x   = reshape( inputs[j,:], :, 1)
	    y   = reshape(targets[j,:], :, 1)
	    ŷ   = neural_network(x, wh, bh, wo, bo)

	    Ewh = dloss_wh(x, y, wh, bh, wo, bo)
	    Ebh = dloss_bh(x, y, wh, bh, wo, bo)
	    Ewo = dloss_wo(x, y, wh, bh, wo, bo)
	    Ebo = dloss_bo(x, y, wh, bh, wo, bo)
	    ∇E .+= vcat(Ewh, Ebh, Ewo, Ebo)
	  end
	  return ∇E/length(train_set)
	end

	function test(parameters, test_set)
	  wh, bh, wo, bo = subviews(parameters,
							    (n_hidden * n_input), (n_hidden),
							    (n_output * n_hidden), (n_output))
	  Et  = zero(0.)
	  for j = test_set
	    x   = reshape( inputs[j,:], :, 1)
	    y   = reshape(targets[j,:], :, 1)

	    Et += loss(x, y, wh, bh, wo, bo)
	  end
	  return Et/length(test_set)
	end

	function optimize!(parameters)
	  parameters .= step!(optimizer,
			    p -> test(p, test_set), p -> train(p, train_set[1:batch_size]),
			    parameters)
	end

	mean_squared_loss(y, ŷ) = sum(0.5(y - ŷ).^2)

	# activation functions
	σ(x) = @. one(x) / (one(x) + exp(-x))
	ReLU(x) = @. max(zero(x), x)
	linear(x) = x
	softmax(x) = exp.(x) ./ sum(exp.(x))
	sigmoid(x) = @. x / (one(x) + exp(-x))

	# dense layer
	dense(w, b, n::Integer, m::Integer, v, activation::Function) =
  			activation(reshape(w, n, m) * v .+ reshape(b, n, 1))

	# params
	n_input = 3
	n_hidden = 12
	n_output = 1
	batch_size = 1
	test_size = 20
	train_size = 100
	epochs = 10000

	parameters = zeros(n_hidden * n_input + n_hidden +
                   	   n_output * n_hidden + n_output)
	start_wh, _, start_wo, _ = subviews(parameters,
  						    (n_hidden*n_input), (n_hidden),
  						    (n_output*n_hidden),(n_output))
	start_wh .= randn(n_hidden*n_input)
	start_wo .= randn(n_output*n_hidden)

	data_size = test_size + train_size
	train_set = shuffle(1:data_size)[1:train_size]
	test_set = setdiff(1:data_size, train_set)
	inputs = rand(data_size, 3)
	targets =  2 * inputs[:, 1] - inputs[:, 2] + inputs[:, 3]


	dloss_wh(x, y, wh, bh, wo, bo) = vec(J(w -> loss(x, y, w,  bh, wo, bo), wh));
	dloss_bh(x, y, wh, bh, wo, bo) = vec(J(b -> loss(x, y, wh, b,  wo, bo), bh));
	dloss_wo(x, y, wh, bh, wo, bo) = vec(J(w -> loss(x, y, wh, bh, w,  bo), wo));
	dloss_bo(x, y, wh, bh, wo, bo) = vec(J(b -> loss(x, y, wh, bh, wo, b),  bo));


	stuff = Tuple{DescentMethod, String, Int32}[]
	n = length(parameters)
	push!(stuff, (Adam(0.1, 0.9, 0.8, 1e-8, 0, zeros(n), zeros(n)), "Adam (lr=0.1)", 112))
	push!(stuff, (Adam(0.01, 0.9, 0.8, 1e-8, 0, zeros(n), zeros(n)), "Adam (lr=0.01)", 847))
	push!(stuff, (Adam(0.001, 0.9, 0.8, 1e-8, 0, zeros(n), zeros(n)), "Adam (lr=0.001)", 8231))
	push!(stuff, (BFGS(Matrix(1.0I, n, n)), "BFGS", 95))
	push!(stuff, (LimitedMemoryBFGS(1, [], [], []), "L-BFGS (m=1)", 5))
	push!(stuff, (LimitedMemoryBFGS(2, [], [], []), "L-BFGS (m=2)", 5))
	push!(stuff, (LimitedMemoryBFGS(3, [], [], []), "L-BFGS (m=3)", 5))

	println("--------------------------------------------------------------")
	for (M, name, N) in stuff
		wh = start_wh
		wo = start_wo
		println(name)
	    @btime pts = run_descent_method(M, N)
	end

	# using Plots
	# 	plt = plot()
	# 	for (M, name, N) in stuff
	# 		println(name)
	# 		N = epochs
		# wh .= randn(n_hidden*n_input)
		# wo .= randn(n_output*n_hidden)
	# 		iters = run_descent_method(M, N)
	# 		plt = plot!(iters, xlabel="Liczba iteracji", ylabel="Wartość funkcji celu",
	# 				label = name,
	# 				ylims=(0, 0.0004)
	# 				)
	# 		display(plt)
	# 		savefig("plots/neural_network/nn_bfgs.png")
	# 	end
