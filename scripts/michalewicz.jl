using LinearAlgebra
include("optimizers.jl")

	# MICHALEWICZ
	f = x -> (sin(x[1])*(sin(x[1]^2 / π))^20 + sin(x[2])*(sin(2*(x[2])^2 / π))^20 +
			  sin(x[3])*(sin(3*(x[3])^2 / π))^20 + sin(x[4])*(sin(4*(x[4])^2 / π))^20)

	∇f = x ->  [cos(x[1])*sin(1*(x[1])^2 / π) + sin(x[1])*20*sin(1*x[1]^2 / π)*cos(1*x[1]^2 / π)*2*x[1] / π,
				cos(x[2])*sin(2*(x[2])^2 / π) + sin(x[2])*20*sin(2*x[2]^2 / π)*cos(2*x[2]^2 / π)*4*x[2] / π,
				cos(x[3])*sin(3*(x[3])^2 / π) + sin(x[3])*20*sin(3*x[3]^2 / π)*cos(3*x[3]^2 / π)*6*x[3] / π,
				cos(x[4])*sin(4*(x[4])^2 / π) + sin(x[4])*20*sin(4*x[4]^2 / π)*cos(4*x[4]^2 / π)*8*x[4] / π]

	x₀ = [-0.5, 1.5, -2, 0.5]

	stuff = Tuple{DescentMethod, String, Int16}[]
	push!(stuff, (Adam(0.1, 0.9, 0.8, 1e-8, 0, zeros(2), zeros(2)), "Adam (lr=0.1)", 122))
	push!(stuff, (Adam(0.01, 0.9, 0.8, 1e-8, 0, zeros(2), zeros(2)), "Adam (lr=0.01)", 339))
	push!(stuff, (Adam(0.001, 0.9, 0.8, 1e-8, 0, zeros(2), zeros(2)), "Adam (lr=0.001)", 1000))
	push!(stuff, (BFGS(Matrix(1.0I, 2, 2)), "BFGS", 22))
	push!(stuff, (LimitedMemoryBFGS(1, [], [], []), "L-BFGS (m=1)", 8))
	push!(stuff, (LimitedMemoryBFGS(2, [], [], []), "L-BFGS (m=2)", 8))
	push!(stuff, (LimitedMemoryBFGS(3, [], [], []), "L-BFGS (m=3)", 8))

	test_btime(stuff, x₀, f, ∇f)

	# path = "plots/michalewicz_4d/test.png"
	# draw_plot(stuff, 1000, x₀, f, ∇f, path, false)
