using LinearAlgebra
include("optimizers.jl")

	# ROSENBROCK 6D
	f = x ->   (100*(x[1]^2-x[2])^2 + (x[1]-1)^2 +
				100*(x[3]^2-x[4])^2 + (x[3]-1)^2 +
				100*(x[5]^2-x[6])^2 + (x[5]-1)^2)

	∇f = x ->  [100*(4*x[1]^3-4*x[2]*x[1]) + x[1]^2-2*x[1]+1, 100*(2*x[1]^2+2*x[2]),
				100*(4*x[3]^3-4*x[4]*x[3]) + x[3]^2-2*x[3]+1, 100*(2*x[3]^2+2*x[4]),
				100*(4*x[5]^3-4*x[6]*x[5]) + x[5]^2-2*x[5]+1, 100*(2*x[5]^2+2*x[6])]

	x₀ = [-0.5, 1.5, -2, 0.5, -1, 0.8]

	stuff = Tuple{DescentMethod, String, Int32}[]
	push!(stuff, (Adam(0.1, 0.9, 0.8, 1e-8, 0, zeros(2), zeros(2)), "Adam (lr=0.1)", 104))
	push!(stuff, (Adam(0.01, 0.9, 0.8, 1e-8, 0, zeros(2), zeros(2)), "Adam (lr=0.01)", 393))
	push!(stuff, (Adam(0.001, 0.9, 0.8, 1e-8, 0, zeros(2), zeros(2)), "Adam (lr=0.001)", 1873))
	push!(stuff, (BFGS(Matrix(1.0I, 2, 2)), "BFGS", 23))
	push!(stuff, (LimitedMemoryBFGS(1, [], [], []), "L-BFGS (m=1)", 4924657))
	push!(stuff, (LimitedMemoryBFGS(2, [], [], []), "L-BFGS (m=2)", 4924657))
	push!(stuff, (LimitedMemoryBFGS(3, [], [], []), "L-BFGS (m=3)", 4924657))

	test_btime(stuff, x₀, f, ∇f)

	# path = "plots/rosenbrock_6d/test.png"
	# draw_plot(stuff, 10000, x₀, f, ∇f, path, false)
