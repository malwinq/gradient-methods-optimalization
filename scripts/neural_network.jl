##################################################
# This script uses Bartosz Chaber's functions
# https://github.com/bchaber/multilayer-perceptron
##################################################

import Random: shuffle!, shuffle
using BenchmarkTools
include("dual.jl")
include("optimizers.jl")
using Plots
using LinearAlgebra

	function subviews(u, ns...)
	   vs, s = [], 0
	   for n in ns
	     N = prod(n)
	     v = reshape(view(u, (1:N) .+ s), n...)
	     s += N
	     push!(vs, v)
	   end
	   vs
	end

	function neural_network(x, wh, bh, wo, bo)
	    x̂ = dense(wh, bh, n_hidden, n_input, x, ReLU)
	    ŷ = dense(wo, bo, n_output, n_hidden, x̂, linear)
	end

	function loss(x, y, wh, bh, wo, bo)
		ŷ = neural_network(x, wh, bh, wo, bo)
		mean_squared_loss(y, ŷ)
	end

	function train(parameters, train_set)
	  wh, bh, wo, bo = subviews(parameters,
							    (n_hidden * n_input), (n_hidden),
							    (n_output * n_hidden), (n_output))
	  ∇E = zeros(length(parameters))
	  for j = train_set
	    x   = reshape( inputs[j,:], :, 1)
	    y   = reshape(targets[j,:], :, 1)
	    ŷ   = neural_network(x, wh, bh, wo, bo)

	    Ewh = dloss_wh(x, y, wh, bh, wo, bo)
	    Ebh = dloss_bh(x, y, wh, bh, wo, bo)
	    Ewo = dloss_wo(x, y, wh, bh, wo, bo)
	    Ebo = dloss_bo(x, y, wh, bh, wo, bo)
	    ∇E .+= vcat(Ewh, Ebh, Ewo, Ebo)
	  end
	  return ∇E/length(train_set)
	end

	function test(parameters, test_set)
	  wh, bh, wo, bo = subviews(parameters,
							    (n_hidden * n_input), (n_hidden),
							    (n_output * n_hidden), (n_output))
	  Et  = zero(0.)
	  for j = test_set
	    x   = reshape( inputs[j,:], :, 1)
	    y   = reshape(targets[j,:], :, 1)

	    Et += loss(x, y, wh, bh, wo, bo)
	  end
	  return Et/length(test_set)
	end

	function optimize!(parameters)
	  parameters .= step!(optimizer,
			    p -> test(p, test_set), p -> train(p, train_set[1:batch_size]),
			    parameters)
	end

	function run_descent_method(M::DescentMethod, N)
		global optimizer = M
		for i=1:N
		  shuffle!(train_set)
		  optimize!(parameters)
		end
		return 0
	end

	function run_descent_method_plot(M::DescentMethod, N)
		global optimizer = M
		iters = []
		for i=1:N
		  shuffle!(train_set)
		  optimize!(parameters)
		  push!(iters, test(parameters, 1:data_size))
		end
		println("Final loss: ", test(parameters, 1:data_size))
		return iters
	end

	function test_btime(stuff)
		for (M, name, N) in stuff
			wh = start_wh
			wo = start_wo
			println(name)
			@btime pts = run_descent_method(M, N)
		end
	end

	function draw_plot(stuff)
		plt = plot()
		for (M, name, N) in stuff
			println(name)
			N = epochs
			iters = run_descent_method_plot(M, N)
			plt = plot!(iters, xlabel="Liczba iteracji", ylabel="Wartość funkcji celu",
					label = name,
					xaxis=:log, yaxis=:log
					)
			display(plt)
			savefig("plots/neural_network/test.png")
		end
	end


	mean_squared_loss(y, ŷ) = sum(0.5(y - ŷ).^2)

	# activation functions
	σ(x) = @. one(x) / (one(x) + exp(-x))
	ReLU(x) = @. max(zero(x), x)
	linear(x) = x
	softmax(x) = exp.(x) ./ sum(exp.(x))
	sigmoid(x) = @. x / (one(x) + exp(-x))

	# dense layer
	dense(w, b, n::Integer, m::Integer, v, activation::Function) =
  			activation(reshape(w, n, m) * v .+ reshape(b, n, 1))

	# params
	n_input = 3
	n_hidden = 12
	n_output = 1
	batch_size = 1
	test_size = 20
	train_size = 100
	epochs = 10000

	parameters = zeros(n_hidden * n_input + n_hidden +
                   	   n_output * n_hidden + n_output)
	start_wh, _, start_wo, _ = subviews(parameters,
  						    (n_hidden*n_input), (n_hidden),
  						    (n_output*n_hidden),(n_output))
	start_wh .= randn(n_hidden*n_input)
	start_wo .= randn(n_output*n_hidden)

	data_size = test_size + train_size
	train_set = shuffle(1:data_size)[1:train_size]
	test_set = setdiff(1:data_size, train_set)
	inputs = rand(data_size, 3)
	targets =  2 * inputs[:, 1] - inputs[:, 2] + inputs[:, 3]

	dloss_wh(x, y, wh, bh, wo, bo) = vec(J(w -> loss(x, y, w,  bh, wo, bo), wh));
	dloss_bh(x, y, wh, bh, wo, bo) = vec(J(b -> loss(x, y, wh, b,  wo, bo), bh));
	dloss_wo(x, y, wh, bh, wo, bo) = vec(J(w -> loss(x, y, wh, bh, w,  bo), wo));
	dloss_bo(x, y, wh, bh, wo, bo) = vec(J(b -> loss(x, y, wh, bh, wo, b),  bo));

	stuff = Tuple{DescentMethod, String, Int32}[]
	n = length(parameters)
	push!(stuff, (Adam(0.1, 0.9, 0.8, 1e-8, 0, zeros(n), zeros(n)), "Adam (lr=0.1)", 112))
	push!(stuff, (Adam(0.01, 0.9, 0.8, 1e-8, 0, zeros(n), zeros(n)), "Adam (lr=0.01)", 847))
	push!(stuff, (Adam(0.001, 0.9, 0.8, 1e-8, 0, zeros(n), zeros(n)), "Adam (lr=0.001)", 8231))
	push!(stuff, (BFGS(Matrix(1.0I, n, n)), "BFGS", 95))
	push!(stuff, (LimitedMemoryBFGS(1, [], [], []), "L-BFGS (m=1)", 5))
	push!(stuff, (LimitedMemoryBFGS(2, [], [], []), "L-BFGS (m=2)", 5))
	push!(stuff, (LimitedMemoryBFGS(3, [], [], []), "L-BFGS (m=3)", 5))

	test_btime(stuff)
	# draw_plot(stuff)
