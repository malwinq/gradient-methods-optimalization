abstract type DescentMethod end
using BenchmarkTools
using Plots

mutable struct Adam <: DescentMethod
    α::Float16     			   # learning rate
    γv::Float64      		   # update decay
    γs::Float64                # gradient decay
    ϵ::Float64                 # very small value
    k::Int32                   # step
    v::Array{Float64, 1}       # 1st moment estimate
    s::Array{Float64, 1}       # 2nd moment estimate
end

function init!(M::Adam, f, ∇f, x)
	M.ϵ = 1e-8
    M.s = zeros(length(x))
    M.v = zeros(length(x))
    return M
end

function step!(M::Adam, f, ∇f, x)
	α, γv, γs, ϵ, k = M.α, M.γv, M.γs, M.ϵ, M.k
	s, v, g = M.s, M.v, ∇f(x)
    @views s[:] = γs * s + (1.0 - γs)*(g .* g)
    @views v[:] = γv * v + (1.0 - γv)*g
    M.k = k += 1
    @views v_hat = v ./ (1.0 - γv^k)
    @views s_hat = s ./ (1.0 - γs^k)
    return x - α*v_hat ./ (sqrt.(s_hat) .+ ϵ)
end

function _line_search(f, x, d)
    @views d::Array{Float64, 1} = normalize(d)
    v::Float64, α::Float64 = f(x), 1e-6
    while f(x + α*d) < v
        v = f(x + α*d)
        α += 1e-6
    end
    return x + α*d
end

mutable struct BFGS <: DescentMethod
    Q::Array{Float64, 2}
end

function init!(M::BFGS, f, ∇f, x)
    M.Q = Matrix(1.0I, length(x), length(x))
    return M
end

function step!(M::BFGS, f, ∇f, x)
    Q, g = M.Q, ∇f(x)
    x′ = _line_search(f, x, -Q*g)
    g′ = ∇f(x′)
    δ = x′ - x
    γ = g′ - g
    @views Q[:] = Q - (δ * γ' * Q + Q * γ * δ') / (δ' * γ) +
           (1 + (γ' * Q * γ) / (δ' * γ))[1] * (δ * δ') / (δ' * γ)
    return x′
end

mutable struct LimitedMemoryBFGS <: DescentMethod
	m::Int8
	δs::Array{Array{Float64, 1}, 1}
	γs::Array{Array{Float64, 1}, 1}
	qs::Array{Array{Float64, 1}, 1}
end

function init!(M::LimitedMemoryBFGS, f, ∇f, x, m=2)
	M.δs = []
	M.γs = []
    M.qs = []
	M.m = m
	return M
end

function step!(M::LimitedMemoryBFGS, f, ∇f, x)
    δs, γs, qs, g = M.δs, M.γs, M.qs, ∇f(x)
    m = length(δs)
    if m > 0
        q = g
        for i in m : -1 : 1
            qs[i] = copy(q)
            q -= (δs[i]⋅q)/(γs[i]⋅δs[i])*γs[i]
        end
        z = (γs[m] .* δs[m] .* q) / (γs[m]⋅γs[m])
        for i in 1 : m
            z += δs[i]*(δs[i]⋅qs[i] - γs[i]⋅z)/(γs[i]⋅δs[i])
        end
        x′ = _line_search(f, x, -z)
    else
        x′ = _line_search(f, x, -g)
    end
    @views g′::Array{Float64, 1} = ∇f(x′)
    push!(δs, x′ - x)
	push!(γs, g′ - g)
    push!(qs, zeros(length(x)))
    while length(δs) > M.m
        popfirst!(δs); popfirst!(γs); popfirst!(qs)
    end
    return x′
end

function run_descent_method(M::DescentMethod, x₀, N, f, ∇f)
	pts = [x₀]
	init!(M, f, ∇f, x₀)
	for i in 1 : N
		step!(M, f, ∇f, pts[end])
		push!(pts, step!(M, f, ∇f, pts[end]))
	end
	return 0
end

function run_descent_method_plot(M::DescentMethod, x₀, N, f, ∇f)
	pts = [x₀]
	iters = []
	init!(M, f, ∇f, x₀)
	for i in 1 : N
		step!(M, f, ∇f, pts[end])
		push!(pts, step!(M, f, ∇f, pts[end]))
		push!(iters, f(pts[end]))
	end
	println(f(pts[end]))
	return iters
end

function test_btime(stuff, x₀, f, ∇f)
	for (M, name, N) in stuff
		println(name)
		@time pts = run_descent_method(M, x₀, N, f, ∇f)
	end
end

function draw_plot(stuff, N, x₀, f, ∇f, path, log)
	plt = plot()
	for (M, name, k) in stuff
		println(name)
		iters = run_descent_method_plot(M, x₀, N, f, ∇f)
		if (log == false)
			plt = plot!(iters, xlabel="Liczba iteracji", ylabel="Wartość funkcji celu",
					label = name)
		else
			plt = plot!(iters, xlabel="Liczba iteracji", ylabel="Wartość funkcji celu",
					label = name,
					xaxis=:log, yaxis=:log)
		end
		display(plt)
		savefig(path)
	end
end
