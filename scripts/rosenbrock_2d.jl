using LinearAlgebra
include("optimizers.jl")

	# ROSENBROCK
	f = x -> (1-x[1])^2 + 100*(4x[2] - x[1]^2)^2
	∇f = x -> [2*(200x[1]^3 - 800x[1]*x[2] + x[1] - 1), -800*(x[1]^2 - 4x[2])]

	x₀ = [-2., 1.5]
	N = Int32(100000)

	stuff = Tuple{DescentMethod, String, Int32}[]
	push!(stuff, (Adam(0.1, 0.9, 0.8, 1e-8, 0, zeros(2), zeros(2)), "Adam, lr=0.1", 210))
	push!(stuff, (Adam(0.01, 0.9, 0.8, 1e-8, 0, zeros(2), zeros(2)), "Adam, lr=0.01", 711))
	push!(stuff, (Adam(0.001, 0.9, 0.8, 1e-8, 0, zeros(2), zeros(2)), "Adam, lr=0.001", 4212))
	push!(stuff, (BFGS(Matrix(1.0I, 2, 2)), "BFGS", 22))
	push!(stuff, (LimitedMemoryBFGS(1, [], [], []), "L-BFGS (m=1)", 22))

	test_btime(stuff, x₀, f, ∇f)

	# path = "plots/rosenbrock_2d/test.png"
	# draw_plot(stuff, 1000, x₀, f, ∇f, path, false)
