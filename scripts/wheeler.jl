using LinearAlgebra
include("optimizers.jl")

	# WHEELER
	f = x ->   (-exp(-(x[1]*x[2]-3)^2 - (x[2]-3)^2))
	∇f = x ->  [f(x) * (-2*x[2]^2*x[1] + 3*x[2]),
				f(x) * (-2*x[1]^2*x[2] + 3*x[1] - 2*x[2] + 3)]

	x₀ = [0.2, 0.3]
	N = Int(10000)

	stuff = Tuple{DescentMethod, String, Int16}[]
	push!(stuff, (Adam(0.1, 0.9, 0.8, 1e-8, 0, zeros(2), zeros(2)), "Adam, lr=0.1", 95))
	push!(stuff, (Adam(0.01, 0.9, 0.8, 1e-8, 0, zeros(2), zeros(2)), "Adam, lr=0.01", 250))
	push!(stuff, (Adam(0.001, 0.9, 0.8, 1e-8, 0, zeros(2), zeros(2)), "Adam, lr=0.001", 1278))
	push!(stuff, (BFGS(Matrix(1.0I, 2, 2)), "BFGS", 198))
	push!(stuff, (LimitedMemoryBFGS(1, [], [], []), "L-BFGS (m=1)", 8))

	test_btime(stuff, x₀, f, ∇f)

	# path = "plots/wheeler_2d/test.png"
	# draw_plot(stuff, 1000, x₀, f, ∇f, path, false)
