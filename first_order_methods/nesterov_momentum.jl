abstract type DescentMethod end

mutable struct NesteovMomentum <: DescentMethod
    α       # learning rate
    β       # momentum decay
    v       # momentum
end

function init!(M::NesterovMomentum, f, ∇f, x)
    M.v = zeros(length(x))
    return M
end

function step!(M::NesteovMomentum, f, ∇f, x)
    α, β, v = M.α, M.β, M.v
    v[:] = β*v - α*∇(x + β*v)
    return x + v
end
