abstract type DescentMethod end

mutable struct Adam <: DescentMethod
    α       # learning rate
    γv      # update decay
    γs      # gradient decay
    ϵ       # very small value
    k       # step
    v       # 1st moment estimate
    s       # 2nd moment estimate
end

function init!(M::Adam, f, ∇f, x)
    M.k = 0
    M.s = zeros(length(x))
    M.v = zeros(length(x))
    return M
end

function step!(M::Adam, f, ∇f, x)
    α, γv, γs, ϵ, k = M.α, M.γv, M.γs, M.ϵ, M.k
    s, v, g = M.s, M.v, ∇f(x)
    s[:] = γs * s + (1 - γs)*(g .* g)
    v[:] = γv * v + (1 - γv)*g
    M.k = k += 1
    v_hat = v ./ (1 - γv^k)
    s_hat = s ./ (1 - γs^k)
    return x - α*v_hat ./ (sqrt.(s_hat) .+ ϵ)
end
