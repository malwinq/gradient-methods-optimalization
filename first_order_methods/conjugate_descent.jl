abstract type DescentMethod end

mutable struct ConjugateGradientDescent <: DescentMethod
    d
    g
end

function init!(M::ConjugateGradientDescent, f, ∇f, x) = M
    M.g = ∇f(x)
    M.d = -M.g
    return M
end

function step!(M::GradientDescent, f, ∇f, x)
    d, g = M.d, M.g
    g' = ∇f(x)
    β = max(0, dot(g', g' - g) / (g*g))
    d' = -g' + β*d
    x' = line_search(f, x, d')
    M.d, M.g = d', g'
    return x'
end
