abstract type DescentMethod end

mutable struct Adadelta <: DescentMethod
    γs      # gradient decay
    γx      # update decay
    ϵ       # very small value
    s       # sum of squared gradients
    u       # sum of squared updates
end

function init!(M::Adadelta, f, ∇f, x)
    M.s = zeros(length(x))
    M.u = zeros(length(x))
    return M
end

function step!(M::Adadelta, f, ∇f, x)
    γs, γx, ϵ, s, u, g = M.γs, M.γx, M.ϵ, M.s, M.u, ∇f(x)
    s[:] += γs * s + (1 - γs)*(g .* g)
    Δx = -(sqrt.(u) .+ ϵ) ./ (sqrt.(s) .+ ϵ) .* g
    u[:] += γx * u + (1 - γx)*(Δx .* Δx)
    return x + Δx
end
