abstract type DescentMethod end

mutable struct Adagrad <: DescentMethod
    α       # learning rate
    ϵ       # very small value
    s       # sum of squared gradients
end

function init!(M::Adagrad, f, ∇f, x)
    M.s = zeros(length(x))
    return M
end

function step!(M::Adagrad, f, ∇f, x)
    α, ϵ, s, g = M.α, M.ϵ, M.s, ∇f(x)
    s[:] += g .* g
    return x - α*g ./ (sqrt.(s) .+ ϵ)
end
