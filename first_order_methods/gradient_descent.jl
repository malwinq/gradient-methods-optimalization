abstract type DescentMethod end

mutable struct GradientDescent <: DescentMethod
    α       # learning rate
end

function init!(M::GradientDescent, f, ∇f, x) = M

function step!(M::GradientDescent, f, ∇f, x)
    α, g = M.α, ∇f(x)
    return x - α*g
end
