abstract type DescentMethod end

mutable struct RMSProp <: DescentMethod
    α       # learning rate
    γ       # decay
    ϵ       # very small value
    s       # sum of squared gradients
end

function init!(M::RMSProp, f, ∇f, x)
    M.s = zeros(length(x))
    return M
end

function step!(M::Momentum, f, ∇f, x)
    α, γ, ϵ, s, g = M.α, M.γ, M.ϵ, M.s, ∇f(x)
    s[:] += γ * s + (1 - γ)*(g .* g)
    return x - α*g ./ (sqrt.(s) .+ ϵ)
end
