abstract type DescentMethod end

mutable struct Momentum <: DescentMethod
    α       # learning rate
    β       # momentum decay
    v       # momentum
end

function init!(M::Momentum, f, ∇f, x)
    M.v = zeros(length(x))
    return M
end

function step!(M::Momentum, f, ∇f, x)
    α, β, v, g = M.α, M.β, M.v, ∇f(x)
    v[:] = β*v - α*g
    return x + v
end
